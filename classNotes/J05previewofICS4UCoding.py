#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 20:56:34 2021
#Success criteria for January
#Here is a preview of the learning for the month of January
#Please learn all of these concepts in computer programming
#during the month of January
@author: wneal
"""

#Understand and write computer code with the following elements:

#Variables
a = 5253
name = "Zhou"

#Output
print(a) #This is a comment. This command prints the variable 'a'
print(name)  #This command prints the variable 'name'

#Input
colour = input("What is your favourite colour?")

#Decision Structure
if colour == "green":
    print("Hey - that is my favourite colour too!")
else:
    print("Ok. Thank you.")

#Loop
for i in range(0,10):
    print(i)
    
    
#Function - this is just like mathematics class
def f(x):
    print(2*x + 5)  #We can do arithmetic with +, -, * and /
    
f(455)