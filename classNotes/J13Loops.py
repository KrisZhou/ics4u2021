#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 20:49:13 2021
Loops

Loops accomplish repetitive tasks in computer programs
for loops and while loops are two particular loops
used in Python and many other languages
@author: wneal
"""

#For loops - try these. What is happening here?
for i in range(10): #
    print(i)
    
for i in range(3,10): #Starts at 3 ... ends at 9 (not 10)
    print(i)
    
for i in range(3,10,2): #The third number indicates how many numbers to skip
    print(i)

for i in range(30,100,15):
    print(i)


#Nested for loops

#Iterating through items with for loops



#While loops
a = 1
while a<10000000:
    print(a)
    a*=2
    a+=1
    

