#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 12:54:23 2021

@author: wneal
"""

maze = [[1,1,0,1,1,1,1],
        [1,0,0,0,1,1,1],
        [1,0,1,0,1,0,0],
        [1,0,1,0,1,0,1],
        [1,0,1,1,1,0,1],
        [1,0,0,0,0,0,1],
        [1,1,1,1,1,1,1]
        ]

startingRow = 0
startingColumn = 2
currentRow = startingRow
currentColumn = startingColumn
targetRow = 2
targetColumn = 6


#While loop for moving through the maze (while location != target):
    #Display the current maze: S for start, T for target, * for location, 1s, 0s
    #Display the current directions available
    #Input a direction
    #If the direction is valid, update location (print a message)
    #Otherwise, don't! (print a message)
